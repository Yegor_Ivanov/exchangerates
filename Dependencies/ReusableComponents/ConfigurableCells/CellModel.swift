
import UIKit

public protocol CellModel {
	static var cellType: ConfigurableCell.Type { get }
	static var cellID: String { get }
}

public extension CellModel {
	static var cellID: String {
		return Self.cellType.cellID
	}
}

public protocol TableOrCollectionViewCell: class {
	var frame: CGRect { get }
	var contentView: UIView { get }
	var backgroundView: UIView? { get set }
	func prepareForReuse()
}

extension UITableViewCell: TableOrCollectionViewCell {}
extension UICollectionViewCell: TableOrCollectionViewCell {}

public typealias CellCallback = (TableOrCollectionViewCell) -> ()
public protocol SelectableCellModel {
	var selectionCallback: CellCallback? { get }
}

public typealias TextFieldGetter = (UITextField?) -> ()
public typealias TypingCallback = (String) -> ()
public protocol TextInputCellModel {
	var typingCallback: TypingCallback? { get }
	var textFieldGetter: TextFieldGetter? { get }
}

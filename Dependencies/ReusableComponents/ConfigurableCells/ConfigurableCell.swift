
import UIKit
public protocol ConfigurableCell: TableOrCollectionViewCell {
	static var cellID: String { get }

	func setup(with genericModel: CellModel)
}

public extension ConfigurableCell {
	static var cellID: String {
		return String(describing: self)
	}
}

public protocol CellFromNib: TableOrCollectionViewCell {
	static var nibName: String { get }
	static var bundleID: String? { get }
}

public extension CellFromNib {
	static var nibName: String {
		return String(describing: self)
	}

	static var bundleID: String? {
		return nil
	}
}

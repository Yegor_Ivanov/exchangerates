
import Foundation

public struct SectionModel: ExpressibleByArrayLiteral {
	public typealias ArrayLiteralElement = CellModel
	
	public var title: String?
	public var cellModels: [CellModel]
	
	public init(with title: String? = nil, cellModels: [CellModel]) {
		self.title = title
		self.cellModels = cellModels
	}
	
	public init(arrayLiteral elements: CellModel...) {
		cellModels = elements
	}
}

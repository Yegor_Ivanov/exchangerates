
import UIKit

open class CollectionViewController: UICollectionViewController, SimplifiedListController {
	open var sections = [SectionModel]() {
		didSet {
			oldSections = oldValue
		}
	}
	
	open var layout: UICollectionViewLayout? {
		get {
			return collectionView!.collectionViewLayout
		}
		set {
			guard let layout = newValue else { return }
			collectionView!.setCollectionViewLayout(layout, animated: false)
		}
	}
	
	open func invalidateLayout() {
		layout?.invalidateLayout()
	}
	
	open func fetchImage(for cell: ConfigurableCell, at indexPath: IndexPath) {
		// for subclasses
	}
	
	// MARK: - Internal
	
	internal var oldSections: [SectionModel]?
	internal var distinctCellTypes = [CellModel.Type]()
	internal var tableOrCollectionView: TableOrCollectionView {
		return collectionView! as TableOrCollectionView
	}
	
	// MARK: - CollectionView Protocols
	
	open override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if let cell = cell as? ConfigurableCell {
			fetchImage(for: cell, at: indexPath)
		}
	}
	
	open override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		collectionView.deselectItem(at: indexPath, animated: true)
		if let model = sections[indexPath.section].cellModels[indexPath.item] as? SelectableCellModel {
			if let cell = collectionView.cellForItem(at: indexPath) {
				model.selectionCallback?(cell)
			}
		}
	}
	
	open override func numberOfSections(in collectionView: UICollectionView) -> Int {
		return sections.count
	}
	
	open override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return sections[section].cellModels.count
	}
	
	open override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let model = sections[indexPath.section].cellModels[indexPath.item]
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: type(of: model).cellID, for: indexPath) as! ConfigurableCell
		cell.setup(with: model)
		return cell as! UICollectionViewCell
	}
}


import UIKit

// MARK: - SimplifiedListController

typealias SimplifiedListController = SimplifiedListControllerBase & ImplicitCellRegistration & DataReloading

public protocol SimplifiedListControllerBase: class {
	var sections: [SectionModel] { get set }
	func reloadData()
	func update()
}

extension SimplifiedListControllerBase {
	public func reloadData() {
		(self as? DataReloading)?.reloadData(animated: false)
	}
	
	public func update() {
		(self as? DataReloading)?.reloadData(animated: true)
	}
}

// MARK: - ImplicitCellRegistration

internal protocol ImplicitCellRegistration: SimplifiedListControllerBase {
	var distinctCellTypes: [CellModel.Type] { get set }
	var tableOrCollectionView: TableOrCollectionView { get }
	func register(_ cellModelTypes: [CellModel.Type])
	func registerCellFromNib(_ cellType: CellFromNib.Type, for cellID: String)
}

extension ImplicitCellRegistration {
	func updateRegisteredCells() {
		for section in sections {
			for cellModelType in (section.cellModels.map { type(of: $0) }) {
				if !distinctCellTypes.contains { $0 == cellModelType } {
					distinctCellTypes.append(cellModelType)
				}
			}
		}
		register(distinctCellTypes)
	}
	
	func register(_ cellModelTypes: [CellModel.Type]) {
		for modelType in cellModelTypes {
			if let nibCellType = modelType.cellType as? CellFromNib.Type {
				registerCellFromNib(nibCellType, for: modelType.cellID)
			} else {
				tableOrCollectionView.registerCellClass(modelType.cellType, for: modelType.cellID)
			}
		}
	}
	
	func registerCellFromNib(_ cellType: CellFromNib.Type, for cellID: String) {
		var bundle: Bundle? = nil
		if let bundleID = cellType.bundleID {
			bundle = Bundle(identifier: bundleID)
		}
		
		let nib = UINib(nibName: cellType.nibName, bundle: bundle)
		tableOrCollectionView.registerNib(nib, for: cellID)
	}
}

// MARK: - DataReloading

internal protocol DataReloading: ImplicitCellRegistration {
	var oldSections: [SectionModel]? { get set }
	func reloadData(animated: Bool)
}

extension DataReloading {
	func reloadData(animated: Bool) {
		updateRegisteredCells()
		
		if animated && oldSections?.count == sections.count {
			tableOrCollectionView.reloadSections(IndexSet(0..<sections.count))
		} else {
			tableOrCollectionView.reloadData()
		}
	}
}

// MARK: - Auxiliary

internal protocol TableOrCollectionView {
	func registerCellClass(_ cellClass: AnyClass?, for cellID: String)
	func registerNib(_ nib: UINib?, for cellID: String)
	func reloadData()
	func reloadSections(_ sections: IndexSet)
}

extension UITableView: TableOrCollectionView {
	func registerCellClass(_ cellClass: AnyClass?, for cellID: String) {
		register(cellClass, forCellReuseIdentifier: cellID)
	}
	
	func registerNib(_ nib: UINib?, for cellID: String) {
		register(nib, forCellReuseIdentifier: cellID)
	}
	
	func reloadSections(_ sections: IndexSet) {
		reloadSections(sections, with: .fade)
	}
}

extension UICollectionView: TableOrCollectionView {
	func registerCellClass(_ cellClass: AnyClass?, for cellID: String) {
		register(cellClass, forCellWithReuseIdentifier: cellID)
	}
	
	func registerNib(_ nib: UINib?, for cellID: String) {
		register(nib, forCellWithReuseIdentifier: cellID)
	}
}

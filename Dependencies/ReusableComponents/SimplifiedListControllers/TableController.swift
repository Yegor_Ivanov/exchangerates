
import UIKit

@objc
open class TableController: NSObject, SimplifiedListController, UITableViewDelegate, UITableViewDataSource {
	@IBOutlet open weak var tableView: UITableView!
	open var sections = [SectionModel]() {
		didSet {
			oldSections = oldValue
		}
	}
	
	open func fetchImage(for cell: ConfigurableCell, at indexPath: IndexPath) {
		// for subclasses
	}
	
	// MARK: - Internal
	
	internal var oldSections: [SectionModel]?
	internal var distinctCellTypes = [CellModel.Type]()
	internal var tableOrCollectionView: TableOrCollectionView {
		return tableView as TableOrCollectionView
	}
	
	// MARK: - TableView Protocols
	
	open func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if let cell = cell as? ConfigurableCell {
			fetchImage(for: cell, at: indexPath)
		}
	}
	
	open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		if let model = sections[indexPath.section].cellModels[indexPath.item] as? SelectableCellModel {
			if let cell = tableView.cellForRow(at: indexPath) {
				model.selectionCallback?(cell)
			}
		}
	}
	
	open func numberOfSections(in tableView: UITableView) -> Int {
		return sections.count
	}
	
	open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return sections[section].cellModels.count
	}
	
	open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let model = sections[indexPath.section].cellModels[indexPath.item]
		let cell = tableView.dequeueReusableCell(withIdentifier: type(of: model).cellID, for: indexPath) as! ConfigurableCell
		cell.setup(with: model)
		return cell as! UITableViewCell
	}
}


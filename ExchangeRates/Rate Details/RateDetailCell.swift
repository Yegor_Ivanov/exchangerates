
import UIKit
import ReusableComponents

struct RateDetailCellModel: CellModel {
	let title: String?
	let value: String?
	static var cellType: ConfigurableCell.Type {
		return RateDetailCell.self
	}
}

class RateDetailCell: UITableViewCell, ConfigurableCell, CellFromNib {
	func setup(with genericModel: CellModel) {
		let model = genericModel as! RateDetailCellModel
		textLabel?.text = model.title
		detailTextLabel?.text = model.value
	}
}

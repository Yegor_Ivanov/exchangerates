
import UIKit
import ReusableComponents

class RateDetailsVC: UIViewController {
	@IBOutlet private var tableController: TableController!
	var rate: Rate?
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		title = rate?.name
		showDetails()
	}
	
	private func showDetails() {
		guard let rate = rate else {
			return
		}
		
		tableController.sections = [
			[
				RateDetailCellModel(title: "Currrent Value", value: rate.valueString),
				RateDetailCellModel(title: "Previous Value", value: rate.previousValueString),
				],
			[
				RateDetailCellModel(title: "Nominal", value: String(describing:rate.nominal)),
				RateDetailCellModel(title: "ISO Code", value: rate.charCode),
				RateDetailCellModel(title: "Numeric Code", value: rate.numCode),
			]
		]
		tableController.reloadData()
	}
}

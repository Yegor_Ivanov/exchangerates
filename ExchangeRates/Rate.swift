
import Foundation

struct RateJson: Codable {
	let rateDetailsDict: [String: Rate]
	
	enum CodingKeys: String, CodingKey {
		case rateDetailsDict = "Valute"
	}
}

struct Rate: Codable {
	let id: String
	let numCode: String
	let charCode: String
	let nominal: Int
	let name: String
	let value: Decimal
	let previousValue: Decimal
	
	enum CodingKeys: String, CodingKey {
		case id = "ID"
		case numCode = "NumCode"
		case charCode = "CharCode"
		case nominal = "Nominal"
		case name = "Name"
		case value = "Value"
		case previousValue = "Previous"
	}
}

extension Rate {
	static let formatter: NumberFormatter = {
		let formatter = NumberFormatter()
		formatter.usesGroupingSeparator = true
		formatter.numberStyle = .currency
		formatter.currencySymbol = ""
		return formatter
	}()
	
	var valueString: String? {
		return Rate.formatter.string(from: NSDecimalNumber(decimal: value))
	}
	
	var previousValueString: String? {
		return Rate.formatter.string(from: NSDecimalNumber(decimal: previousValue))
	}
}

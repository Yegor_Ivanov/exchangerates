
import UIKit
import ReusableComponents

struct RateCellModel: CellModel, SelectableCellModel {
	var iso: String?
	var rate: String?
	var selectionCallback: CellCallback?
	
	static var cellType: ConfigurableCell.Type {
		return RateCell.self
	}
}

class RateCell: UICollectionViewCell, ConfigurableCell, CellFromNib {
	@IBOutlet private var isoLabel: UILabel!
	@IBOutlet private var rateLabel: UILabel!
	@IBOutlet private var imageView: UIImageView!
	var model: RateCellModel?
	
	func setup(with genericModel: CellModel) {
		updateImage(nil)
		guard let model = genericModel as? RateCellModel else {
			return
		}
		
		self.model = model
		isoLabel.text = model.iso
		rateLabel.text = model.rate
	}
	
	func updateImage(_ image: UIImage?) {
		imageView.image = image
	}
}

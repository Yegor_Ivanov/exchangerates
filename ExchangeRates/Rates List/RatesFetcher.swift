
import UIKit
import Alamofire
import AlamofireImage

protocol RatesFetcherDelegate: class {
	func present(_ rates: [Rate])
	func handle(_ error: Error)
}

internal class RatesFetcher {
	weak var delegate: RatesFetcherDelegate?
	
	func fetchList() {
		let path = "https://www.cbr-xml-daily.ru/daily_json.js"
		Alamofire.request(path).responseData { response in
			switch response.result {
			case .success(let jsonData):
				self.parse(jsonData)
			case .failure(let error):
				self.delegate?.handle(error)
			}
		}
	}
	
	func fetchImage(for iso: String, callback: @escaping (UIImage?) -> ()) {
		let path = "https://raw.githubusercontent.com/transferwise/currency-flags/master/src/flags/\(iso.lowercased()).png"
		Alamofire.request(path).responseImage { response in
			callback(response.result.value)
		}
	}
	
	private func parse(_ jsonData: Data) {
		let decoder = JSONDecoder()
		do {
			let rateJson = try decoder.decode(RateJson.self, from: jsonData)
			delegate?.present(Array(rateJson.rateDetailsDict.values))
		}
		catch {
			delegate?.handle(error)
		}
	}
}

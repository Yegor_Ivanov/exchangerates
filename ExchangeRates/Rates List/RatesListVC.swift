
import UIKit
import ReusableComponents

class RatesListVC: CollectionViewController, UICollectionViewDelegateFlowLayout, RatesFetcherDelegate {
	private let refreshControl = UIRefreshControl()
	private let fetcher = RatesFetcher()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setup()
		fetch()
	}
	
	private func setup() {
		title = "Rates"
		refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: .valueChanged)
		collectionView?.refreshControl = refreshControl
		installsStandardGestureForInteractiveMovement = true
		fetcher.delegate = self
	}
	
	@objc private func didPullToRefresh(_ sender: UIRefreshControl) {
		fetch()
	}
	
	private func presentRateScreen(for rate: Rate) {
		guard let rateDetailsVC = storyboard?.instantiateViewController(withIdentifier: "RateDetailsVC") as? RateDetailsVC else {
			return
		}
		rateDetailsVC.rate = rate
		navigationController?.pushViewController(rateDetailsVC, animated: true)
	}
	
	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		layout?.invalidateLayout()
	}
	
	// MARK: - Fetching
	
	func fetch() {
		refreshControl.beginRefreshing()
		fetcher.fetchList()
	}
	
	func present(_ rates: [Rate]) {
		refreshControl.endRefreshing()
		let cells = rates.map { cellModel(for: $0) }
		let section = SectionModel(cellModels: cells)
		sections = [section]
		reloadData()
	}
	
	func handle(_ error: Error) {
		refreshControl.endRefreshing()
		let errorMessage: String
		switch error {
		case is DecodingError:
			errorMessage = "Decoding Error"
		default:
			errorMessage = error.localizedDescription
		}
		
		let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		present(alert, animated: true, completion: nil)
	}
	
	
	// MARK: - Collection View
	
	private func cellModel(for rate: Rate) -> CellModel {
		var cellModel = RateCellModel()
		cellModel.iso = rate.charCode
		cellModel.rate = rate.valueString
		cellModel.selectionCallback = { [unowned self] _ in
			self.presentRateScreen(for: rate)
		}
		return cellModel
	}
	
	override func fetchImage(for cell: ConfigurableCell, at indexPath: IndexPath) {
		guard
			let cell = cell as? RateCell,
			let cellModel = sections[indexPath.section].cellModels[indexPath.item] as? RateCellModel,
			let iso = cellModel.iso
		else { return }
		
		fetcher.fetchImage(for: iso) { image in
			if cell.model?.iso == cellModel.iso {
				cell.updateImage(image)
			}
		}
	}
	
	override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
		guard var cells = sections.first?.cellModels else {
			return
		}
		cells.swapAt(sourceIndexPath.item, destinationIndexPath.item)
		sections = [SectionModel(cellModels: cells)]
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		var divider: CGFloat = 3
		if collectionView.traitCollection.verticalSizeClass == .regular {
			divider = 2
		}
		let width = collectionView.bounds.width / divider
		let height = width
		return CGSize(width: width, height: height)
	}
}

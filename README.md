The project uses [Alamofire](https://github.com/Alamofire/Alamofire) and  [AlamofireImage](https://github.com/Alamofire/AlamofireImage), integrated with [Carthage](https://github.com/Carthage/Carthage).
To install dependencies, after checkout run

	$ carthage bootstrap


If you need to install Carthage first, run

	$ brew update
	$ brew install carthage

